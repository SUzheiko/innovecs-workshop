import { createMuiTheme } from "@material-ui/core/styles";
import colors from "@constants/colors";

const theme = createMuiTheme({
  overrides: {
    MuiButton: {
      contained: {
        boxShadow: "none",
      },
      containedSecondary: {
        backgroundColor: colors.primary.white,
        "&:hover": {
          backgroundColor: colors.secondary.pink,
        },
      },
    },
  },
  typography: {
    fontFamily: "'Roboto', 'Yeseva One', sans-serif",
    fontWeightLight: 300,
    fontWeightRegular: 400,
    fontWeightBold: 500,
    body1: {
      fontFamily: "Roboto",
      fontWeight: 600,
    },
  },
  palette: {
    primary: {
      main: colors.primary.red,
      contrastText: colors.primary.white,
    },
    secondary: {
      main: colors.primary.red,
      contrastText: colors.primary.black,
    },
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 960,
      lg: 1280,
      xl: 1920,
    },
  },
});

export default theme;
