import styled, { css } from "styled-components";

export const AppStyled = styled.main`
  height: 100%;
  overflow-x: auto;
  min-height: 100%;
  font-family: "Roboto", cursive;
  position: relative;
  box-sizing: border-box;
`;

interface IFlex {
  justifyCenter?: boolean;
  justifyBetween?: boolean;
  justifyEvenly?: boolean;
  alignCenter?: boolean;
  alignStart?: boolean;
  alignEnd?: boolean;
  alignBaseline?: boolean;
  flexStart?: boolean;
  flexEnd?: boolean;
  flex?: string;
  flexWrap?: boolean;
  direction?: "column" | "column-reverse" | "row" | "row-reverse";
}

export const Flex = styled.div<IFlex>`
  display: flex;

  ${({ flex }) =>
    flex &&
    css`
      flex: ${flex};
    `}
  ${({ flexWrap }) =>
    flexWrap &&
    css`
      flex-wrap: wrap;
    `}
  ${({ justifyBetween }) =>
    justifyBetween &&
    css`
      justify-content: space-between;
    `}
  ${({ justifyCenter }) =>
    justifyCenter &&
    css`
      justify-content: center;
    `}
  ${({ justifyEvenly }) =>
    justifyEvenly &&
    css`
      justify-content: space-evenly;
    `}
  ${({ alignCenter }) =>
    alignCenter &&
    css`
      align-items: center;
    `}
  ${({ alignStart }) =>
    alignStart &&
    css`
      align-items: flex-start;
    `}
  ${({ alignEnd }) =>
    alignEnd &&
    css`
      align-items: flex-end;
    `}
  ${({ alignBaseline }) =>
    alignBaseline &&
    css`
      align-items: baseline;
    `}
  ${({ flexStart }) =>
    flexStart &&
    css`
      justify-content: flex-start;
    `}
  ${({ flexEnd }) =>
    flexEnd &&
    css`
      justify-content: flex-end;
    `}
  ${({ direction }) =>
    direction &&
    css`
      flex-direction: ${direction};
    `}
`;
