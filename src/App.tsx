import React from "react";
import { hot } from "react-hot-loader";
import { Wrapper } from "./Wrapper";
import { ThemeProvider, StylesProvider } from "@material-ui/core/styles";
import theme from "@styles/theme";

@hot(module)
export class App extends React.Component {
  render() {
    return (
      <StylesProvider injectFirst>
        <ThemeProvider theme={theme}>
          <Wrapper />
        </ThemeProvider>
      </StylesProvider>
    );
  }
}
