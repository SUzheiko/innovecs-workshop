import styled from "styled-components";
import { BREAKPOINT } from "@constants";
import colors from "@constants/colors";

export const FooterWrapper = styled.div`
  display: flex;
  width: 100%;
  font-family: "Roboto", sans-serif;
  justify-content: space-around;
  border-top: 1px solid #cccccc;
`;

export const LogoWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 40px 0px 30px 0px;
  background: ${colors.primary.red};
  @media (max-width: ${BREAKPOINT}) {
    padding: 20px 0px 15px 0px;
  }
`;

export const FlexColumnWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0 50px;
  @media (max-width: ${BREAKPOINT}) {
    padding: 0 10px;
  }
`;

export const Title = styled.div`
  text-align: center;
  font-weight: 700;
  padding: 20px 0px 15px 0px;
  font-size: 1rem;
`;

export const TextWrapper = styled.div`
  font-weight: 400;
  line-height: 30px;
  font-size: 13px;
  display: flex;
  white-space: nowrap;
`;

export const Logo = styled.img`
  width: 87px;
`;

export const IconWrapper = styled.div`
  display: flex;
  width: 25px;
  justify-content: center;
  align-items: center;
`;
