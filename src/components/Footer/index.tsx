import React from "react";
import {
  FooterWrapper,
  LogoWrapper,
  Title,
  TextWrapper,
  FlexColumnWrapper,
  IconWrapper,
} from "./styled";
import { useMediaQuery, Link } from "@material-ui/core";
import { BREAKPOINT } from "@constants";
import LocationIcon from "@icons/location.svg";
import { Flex } from "@styles/styled";
import InnovecsIcon from "@icons/innovecs.svg";

export const Footer = () => {
  const isMobile = useMediaQuery(`(max-width: ${BREAKPOINT})`);

  if (isMobile) {
    return (
      <FooterWrapper>
        <Flex justifyCenter direction="column">
          <Title>Контакты</Title>
          <Flex justifyEvenly>
            <FlexColumnWrapper>
              <TextWrapper>
                <IconWrapper>
                  <LocationIcon />
                </IconWrapper>
                <Link
                  style={{ textDecoration: "underline" }}
                  color="inherit"
                  href={"https://goo.gl/maps/61G5oKAfPiXo6cU18"}
                >
                  <TextWrapper>Киев, б-р Вацлава Гавела, 6з</TextWrapper>
                </Link>
              </TextWrapper>
            </FlexColumnWrapper>
          </Flex>
        </Flex>
      </FooterWrapper>
    );
  }

  return (
    <FooterWrapper>
      <Flex justifyCenter>
        <FlexColumnWrapper>
          <LogoWrapper>
            <InnovecsIcon />
          </LogoWrapper>
        </FlexColumnWrapper>
        <FlexColumnWrapper>
          <Title>Контакты</Title>
          <TextWrapper>
            <IconWrapper>
              <LocationIcon />
            </IconWrapper>
            <Link
              style={{ textDecoration: "underline" }}
              color="inherit"
              href={"https://goo.gl/maps/61G5oKAfPiXo6cU18"}
            >
              <TextWrapper>Киев, б-р Вацлава Гавела, 6з</TextWrapper>
            </Link>
          </TextWrapper>
        </FlexColumnWrapper>
      </Flex>
    </FooterWrapper>
  );
};
