import { GridLoader } from "react-spinners";
import React from "react";

interface IProps {
  loading: boolean;
}

export const Busy = ({ loading }: IProps) => {
  return (
    <div className={"busy-container" + (loading ? "" : " invisible")}>
      <div className="busy">
        <GridLoader color={"#d0006f"} loading={loading} />
      </div>
    </div>
  );
};
