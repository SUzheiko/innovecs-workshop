import React from "react";
import { Switch, Route, Redirect } from "react-router";
import { AppStyled } from "@styles/styled";
import { BrowserRouter as Router } from "react-router-dom";

export function Wrapper() {
  return (
    <AppStyled>
      <Router>
        <Switch>
          <Route path="/404" component={() => <div>Not Found</div>} />
          <Route path="*" component={() => <Redirect to="/404" />} />
        </Switch>
      </Router>
    </AppStyled>
  );
}
