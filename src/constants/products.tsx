export enum ProductType {
  Clothes = "Одежда",
  Souvenir = "Сувениры",
}

export interface IProduct {
  id: string;
  price: number;
  imageUrl?: string;
  type: ProductType;
  isActive?: boolean;
  color?: string;
}

export enum Clothes {
  TShirt = "Футболка",
  HoodieGrey = "Худи Серый",
  HoodieRed = "Худи Красный",
}

export enum Souvenir {
  Bag = "Сумка",
  BookDigitalUkraine = "Книга Digital Ukraine",
  Cup = "Кружка",
  NotebookBlack = "Блокнот Чёрный",
  NotebookWhite = "Блокнот Белый",
  Pen = "Ручка",
  Powerbank = "Powerbank",
  WaterBottleBlack = "Бутылка для воды чёрная",
  WaterBottleRed = "Бутылка для воды красная",
}

export const clothes: IProduct[] = [
  {
    id: Clothes.TShirt,
    imageUrl: "./images/pages/clothes/hoodie_grey.jpeg",
    type: ProductType.Clothes,
    price: 250,
  },
  {
    id: Clothes.HoodieGrey,
    imageUrl: "./images/pages/clothes/hoodie_red.png",
    type: ProductType.Clothes,
    price: 450,
  },
  {
    id: Clothes.HoodieRed,
    imageUrl: "./images/pages/clothes/t_shirt.png",
    type: ProductType.Clothes,
    price: 450,
  },
];

export const souvenirs: IProduct[] = [
  {
    id: Souvenir.Bag,
    imageUrl: "./images/pages/souvenirs/bag.jpeg",
    type: ProductType.Souvenir,
    price: 16,
  },
  {
    id: Souvenir.BookDigitalUkraine,
    imageUrl: "./images/pages/souvenirs/book_digital_ukraine.png",
    type: ProductType.Souvenir,
    price: 16,
  },
  {
    id: Souvenir.Cup,
    imageUrl: "./images/pages/souvenirs/cup.jpg",
    type: ProductType.Souvenir,
    price: 16,
  },
  {
    id: Souvenir.NotebookBlack,
    imageUrl: "./images/pages/souvenirs/notebook_black.jpg",
    type: ProductType.Souvenir,
    price: 16,
  },
  {
    id: Souvenir.NotebookWhite,
    imageUrl: "./images/pages/souvenirs/notebook_white.jpg",
    type: ProductType.Souvenir,
    price: 16,
  },
  {
    id: Souvenir.Pen,
    imageUrl: "./images/pages/souvenirs/pen.png",
    type: ProductType.Souvenir,
    price: 16,
  },
  {
    id: Souvenir.Powerbank,
    imageUrl: "./images/pages/souvenirs/powerbank.png",
    type: ProductType.Souvenir,
    price: 16,
  },
  {
    id: Souvenir.WaterBottleBlack,
    imageUrl: "./images/pages/souvenirs/water_bottle_black.jpg",
    type: ProductType.Souvenir,
    price: 16,
  },
  {
    id: Souvenir.WaterBottleRed,
    imageUrl: "./images/pages/souvenirs/water_bottle_red.png",
    type: ProductType.Souvenir,
    price: 16,
  },
];

export const allDesserts = [...clothes, ...souvenirs];
