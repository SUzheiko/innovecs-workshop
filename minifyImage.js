const imagemin = require("imagemin");
const im = require("imagemagick");
const async = require("async");
const fs = require("fs");
const path = require("path");

function resize(params) {
  const queue = async.queue(resizeimg, 50);

  fs.readdir(params.src, function(err, files) {
    files
      .filter(function(file) {
        return path.extname(file).toLowerCase() === ".jpg";
      })
      .forEach(function(file) {
        queue.push({
          src: path.join(params.src, "/", file),
          dest: path.join(params.dest, "/", file),
          width: params.width,
          height: params.height,
        });
      });
  });
}

function resizeimg(params) {
  const imOptions = {
    srcPath: params.src,
    dstPath: params.dest,
  };
  if (params.width !== undefined) {
    imOptions.width = params.width;
  }
  if (params.height !== undefined) {
    imOptions.height = params.height;
  }
  im.resize(imOptions);
}

(async () => {
  resize({
    src: "./public/images/pages/clothes",
    dest: "./public/images/pages/clothes/small",
    width: 80,
  });

  resize({
    src: "./public/images/pages/souvenirs",
    dest: "./public/images/pages/souvenirs/small",
    width: 80,
  });
})();
